ARG JSON_SERVER_VERSION=0.17.3
FROM node:19-alpine

RUN npm install --global json-server@${JSON_SERVER_VERSION}

EXPOSE 3000
ENTRYPOINT ["json-server"]
CMD ["--host", "0.0.0.0"]

