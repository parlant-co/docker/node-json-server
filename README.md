This is a Docker build for [json-server](https://www.npmjs.com/package/json-server).

You can use it to build quick REST API http servers.
In a Kubernetes setting, this is quite helpful in order to provide simple services that respond
with a JSON API based on the contents of a ConfigMap mounted as a volume.

This is sometimes helpful on its own, or as a sidecar. For example Caddy 2 uses a dynamic endpoint
to query the whitelisted domains to generate SSL certificates for.
Using this json-server as a sidecar, allows you to provide the whitelist as a volume mounted ConfigMap.

